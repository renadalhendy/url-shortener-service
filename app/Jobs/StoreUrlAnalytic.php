<?php

namespace App\Jobs;

use App\Models\UrlAnalytic;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreUrlAnalytic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shortLinkId;
    protected $country;
    protected $browser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($shortLinkId, $country, $browser)
    {
        $this->shortLinkId = $shortLinkId;
        $this->country = $country;
        $this->browser = $browser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        UrlAnalytic::create([
            'short_link_id' => $this->shortLinkId,
            'country' => $this->country,
            'browser' => $this->browser
        ]);
    }
}
