<?php

namespace App\Http\Controllers;

use App\Jobs\StoreUrlAnalytic;
use Illuminate\Http\Request;
use App\Models\ShortLink;
use App\Models\UrlAnalytic;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class ShortLinkController extends Controller
{
    /**
     * Display a listing of the recource.
     * 
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $shortLinks = Cache::remember('shortLinks', 60, function () {
            return ShortLink::with('urlAnalytics')->latest()->get();
        });
        return view('shortenLink', ['shortLinks' => $shortLinks]);
    }

    /**
     * Create shortend link.
     * 
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $request->validate([
            'link'=>'required|url'
        ]);
        $input['link']=$request->link;
        $input['code']= Str::random(6);
        ShortLink::create($input);
        //forget the cache when there is a modification on the database
        //we can use laravel observers but I think this is enough for a simpler project
        Cache::forget('shortLinks');
        return redirect('generate-shorten-link')
            ->with('success','Shorten Link Generated Successfully!');
    }

    /**
     * @param string $code
     * 
     */
    public function shortenLink($code){
        $shortUrl = ShortLink::where('code',$code)->first();
        if($shortUrl){
            $shortUrl->visits++;
            $shortUrl->save();
        }
        // dispatch a job to store analytics data
        dispatch(new StoreUrlAnalytic($shortUrl->id, $this->getCountry(), $this->getBrowser()));
     
        return redirect($shortUrl->link);
    }
    /**
     * get the country of the person making the request to access the url
     * 
     * @return string country or unknown if it's not present
     */
    function getCountry() {
        //in case if the ip address is frobiddened 
        try {
            $ip = $_SERVER['REMOTE_ADDR'];
            //function to retrieve information about the user's IP address from the ipinfo.io API
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            return property_exists($details, 'country') ? $details->country : 'Unknown';
        } catch (Exception $e) {
            return 'Unknown';
        }
    }
    /**
     * get the browser of the person making the request to access the url
     * 
     * @return string $browser 
     */
    function getBrowser() {
        //the $_SERVER superglobal to retrieve the user agent string.
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $browser = "Unknown";
        $browserArray = array(
            '/msie/i'      =>  'Internet Explorer',
            '/firefox/i'   =>  'Firefox',
            '/safari/i'    =>  'Safari',
            '/chrome/i'    =>  'Chrome',
            '/opera/i'     =>  'Opera',
            '/netscape/i' =>  'Netscape',
            '/maxthon/i'   =>  'Maxthon',
            '/konqueror/i' =>  'Konqueror',
            '/mobile/i'    =>  'Handheld Browser'
        );
        foreach ($browserArray as $regex => $value) {
            if (preg_match($regex, $userAgent)) {
                $browser = $value;
            }
        }
        return $browser;
    }
}
