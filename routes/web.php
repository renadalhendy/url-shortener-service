<?php

use App\Http\Controllers\ShortLinkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//get all shorten links to present it in shortenLink view
Route::get('generate-shorten-link',[ShortLinkController::class,'index']);
//post the link to the database to generate shorten link
Route::post('generate-shorten-link',[ShortLinkController::class,'store'])->name('generate.shorten.link.post')->middleware(['throttle:60,1']);
//redirect to the original link when clicking on the shorten link with a middleware to limit requests to 60 per minute
Route::get('{code}',[ShortLinkController::class,'shortenLink'])->name('shorten.link')->middleware(['throttle:60,1']);