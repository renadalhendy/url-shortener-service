<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <title>
            Create a short url
        </title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">Enter Your URL</h1>
            <div class="card">
                <div class="card-header">
                    <form action="{{ route('generate.shorten.link.post') }} " method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" name="link" class="form-control" placeholder="Enter your URl" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">Generate shorten Link</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    {{-- return a success message when the link is shortend --}}
                    @if (Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ Session::get('success') }}</p>
                    </div>  
                    @endif
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Short Link</th>
                                <th>Link</th>
                                <th>Number of visits</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shortLinks as $shortlink )
                            <tr>
                                {{-- so the records are numbered correctly hence we use latest() in the controller --}}
                                <td>{{ $loop->index + 1 }}</td>
                                <td><a href="{{ route('shorten.link', $shortlink->code) }}" target="_blank">{{ route('shorten.link', $shortlink->code) }}</a></td>
                                <td>{{ $shortlink->link }}</td>
                                <td>{{ $shortlink->visits }}</td>
                                <td>
                                @foreach ($shortlink->urlAnalytics as $analytic)
                                @if($analytic->count() > 0)
                                <p> Country: {{ $analytic->country }} Used Broswer: {{ $analytic->browser }} </p>
                                @else
                                <p>Nothing was found</p>
                                @endif
                                @endforeach
                                </td>
                            </tr>
                                
                            @endforeach
                        </tbody>

                    </table>

                </div>

            </div>
        </div>

    </body>
</html>