<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlAnalytic extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'short_link_id',
        'country',
        'browser'
     ];
     /**
      * one to many relationship between shorten url and url analytics
      */
    public function shortLink()
     {
        return $this->belongsTo(ShortLink::class);
     }
}
