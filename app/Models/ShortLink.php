<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShortLink extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     * @var array
     */

    protected $fillable = [
        'code','link','visits'
    ];
    /**
     * hasmany relation demonstrates th one to many relation between ShortLink and UrlAnalytic
     */
    public function urlAnalytics()
    {
        return $this->hasMany(UrlAnalytic::class);
    }
}
